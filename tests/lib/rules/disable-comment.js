"use strict";

const rule = require("../../../lib/rules/disable-comment"),
  RuleTester = require("eslint").RuleTester;

RuleTester.setDefaultConfig({
  parserOptions: {
    ecmaVersion: 6,
    sourceType: "module"
  }
});

const ruleTester = new RuleTester();
ruleTester.run("disable-comment", rule, {
  valid: [
    "eval();",
    "alert();",
    "var a = 1;",
    "let a = 1;"
  ],
  invalid: [
    {
      code: '// eslint-disable-next-line',
      errors: [{
        message: "Disable comment at line 1:0",
      }]
    },
    {
      code: '// eslint-disable-next-line no-console',
      errors: [{
        message: "Disable comment at line 1:0",
      }]
    },
    {
      code: '// eslint-disable-next-line id-length',
      errors: [{
        message: "Disable comment at line 1:0",
      }]
    },
    {
      code: '// eslint-disable-next-line max-len',
      errors: [{
        message: "Disable comment at line 1:0",
      }]
    },
    {
      code: '// eslint-disable',
      errors: [{
        message: "Disable comment at line 1:0",
      }]
    },
    {
      code: '/* eslint-disable-next-line */',
      errors: [{
        message: "Disable comment at line 1:0",
      }]
    },
    {
      code: '/* eslint-disable-next-line no-console */',
      errors: [{
        message: "Disable comment at line 1:0",
      }]
    },
    {
      code: '/* eslint-disable-next-line id-length */',
      errors: [{
        message: "Disable comment at line 1:0",
      }]
    },
    {
      code: '/* eslint-disable-next-line max-len */',
      errors: [{
        message: "Disable comment at line 1:0",
      }]
    },
    {
      code: '/* eslint-disable */',
      errors: [{
        message: "Disable comment at line 1:0",
      }]
    },
    {
      code: 'eval(); // eslint-disable-next-line',
      errors: [{
        message: "Disable comment at line 1:8",
      }]
    },
    {
      code: 'eval(); // eslint-disable-next-line no-console',
      errors: [{
        message: "Disable comment at line 1:8",
      }]
    },
    {
      code: 'eval(); // eslint-disable-next-line id-length',
      errors: [{
        message: "Disable comment at line 1:8",
      }]
    },
    {
      code: 'eval(); // eslint-disable-next-line max-len',
      errors: [{
        message: "Disable comment at line 1:8",
      }]
    },
    {
      code: 'eval(); // eslint-disable',
      errors: [{
        message: "Disable comment at line 1:8",
      }]
    },
    {
      code: 'eval(); /* eslint-disable */',
      errors: [{
        message: "Disable comment at line 1:8",
      }]
    },
    {
      code: 'eval(); /* eslint-disable sort-keys */',
      errors: [{
        message: "Disable comment at line 1:8",
      }]
    },
  ]
});