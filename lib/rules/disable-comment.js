'use strict';

//Based on https://github.com/sindresorhus/eslint-plugin-unicorn/blob/master/rules/no-abusive-eslint-disable.js

// It's a eslint-disable comment
const disableRegex = /^eslint-disable(-next-line|-line)?/;

module.exports = context => ({
  Program: node => {
    for (const comment of node.comments) {
      const value = comment.value.trim();
      const res = disableRegex.exec(value);

      if (res) {
        context.report({
          // Can't set it at the given location as the warning
          // will be ignored due to the disable comment
          loc: {
            line: 0,
            column: 0
          },
          // So specify the location in the message
          message: 'Disable comment at line {{line}}:{{column}}',
          data: comment.loc.start
        });
      }
    }
  }
});